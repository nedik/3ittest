<?php

use App\Route;
use App\Router;

define('ROOT_DIR', __DIR__ . '/');
require_once(__DIR__.'/vendor/autoload.php');

Router::addRoute(new Route('/', 'homepage'));
Router::addRoute(new Route('/saveincommingfile', 'homepage', 'saveFile'));
Router::addRoute(new Route('/dataview', 'dataview'));

App\Bootstrap::start();
