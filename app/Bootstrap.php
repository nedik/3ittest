<?php
namespace App;

use dibi;
use Exception;
use Tracy\Debugger;

class Bootstrap
{
    /**
     * @throws \Dibi\Exception
     */
    public static function start()
    {
        session_start();

        Debugger::$logDirectory = ROOT_DIR . '/logs';
        Debugger::$strictMode = true;
        Debugger::$maxDepth = 5;
        Debugger::$maxLength = 99999;
        Debugger::enable(Debugger::DEVELOPMENT);

        $dibi = dibi::connect([
            'driver'   => 'mysqli',
            'host'     => Config::get('db_host'),
            'username' => Config::get('db_user'),
            'password' => Config::get('db_password'),
            'database' => Config::get('db_name'),
        ]);

        $panel = new \Dibi\Bridges\Tracy\Panel;
        $panel->register($dibi);



        try {
            $route = Router::processRoutes();

            $controllerClass = '\\App\\Controllers\\' . ucfirst($route->getController());
            if (!class_exists($controllerClass)) {
                throw new Exception('Class for route ('.$controllerClass.') not exists');
            }
            $class = new $controllerClass;
            if (!method_exists($class, $route->getAction())) {
                throw new Exception('Method "'.$route->getAction().'" not exists in class "'.$controllerClass.'"');
            }

            View::setTemplateFile($route->getController().'/'.$route->getAction().'.php');

            $result = $class->{$route->getAction()}();
            if (is_array($result)) {
                View::setTemplateParams($result);
            }

            if (View::$redirect) {
                View::$redirect->processRedirect();
            }

            View::render();

        } catch(Exception $e) {
            // some log proccess - not implemented

            throw $e;
        }

    }
}