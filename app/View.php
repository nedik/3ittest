<?php

namespace App;

use App\Exceptions\NoTemplateFoundException;

class View
{
    public static $layoutTemplate = 'layout/main.php';
    public static $content;
    public static $title;
    public static $description;
    /**
     * @var array
     */
    public static $templateParams;
    /**
     * @var string
     */
    public static $templateFile;
    /**
     * @var null|Message
     */
    public static $message;
    /**
     * @var Redirect
     */
    public static $redirect;

    public static function render()
    {
        if (isset($_SESSION['message'])) {
            self::$message = $_SESSION['message'];
            unset($_SESSION['message']);
        }

        $content = '';
        if (self::$templateFile) {
            try {
                $content = (new Template(self::$templateFile))->render(self::$templateParams);
            } catch (NoTemplateFoundException $e) {
                bdump('No template found: '.self::$templateFile);
                // some log, bud is not necessary right now
            }
        }

        $template = new Template(self::$layoutTemplate);
        echo $template->render([
            'content' => $content,
            'message' => self::$message,
        ]);
    }

    public static function setTemplateParams(array $params)
    {
        self::$templateParams = $params;
    }

    public static function setTemplateFile(string $templateFile)
    {
        self::$templateFile = $templateFile;
    }

    /**
     * @param string $url
     * @return Redirect
     */
    public static function redirect(string $url): Redirect
    {
        self::$redirect = new Redirect($url);

        return self::$redirect;
    }
}