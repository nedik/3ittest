<?php

namespace App;

class Redirect
{
    /**
     * @var Message
     */
    protected $message;
    protected $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function withMessage($msg, $type = Message::SUCCESS)
    {
        $this->message = new Message($msg, $type);
    }

    public function processRedirect()
    {
        if ($this->message) {
            $_SESSION['message'] = $this->message;
        }

        header('Location: ' . $this->url);
        exit();
    }
}