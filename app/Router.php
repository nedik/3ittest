<?php

namespace App;

class Router
{
    protected static $routes;
    /**
     * @var string
     */
    public static $url;
    /**
     * @var string
     */
    public static $urlQuery;
    /**
     * @var array
     */
    public static $urlParams;

    public static function addRoute(Route $route)
    {
        self::$routes[$route->getController()][$route->getAction()] = $route;
    }


    protected static function init()
    {
        self::$urlQuery = $_SERVER['QUERY_STRING'];
        self::$urlParams = $_GET;
        $index = strpos($_SERVER['REQUEST_URI'], '?');
        $_SERVER['REQUEST_PATH'] = (($index === false) ? $_SERVER['REQUEST_URI'] : substr($_SERVER['REQUEST_URI'], 0, $index));
        self::$url = strtolower($_SERVER['REQUEST_PATH']);
    }

    /**
     * @return Route
     */
    public static function processRoutes(): Route
    {
        self::init();
        /** @var Route $route */
        foreach (self::$routes as $controllers) {
            foreach ($controllers as $route) {
                if ($route->getUrl() == self::$url) {
                    return $route;
                }
            }
        }

        return new Route('', 'notFound404');
    }


    /**
     * @param $controller
     * @param string $action
     * @param array $data
     * @return string
     * @throws \Exception
     */
    public static function url($controller, string $action = 'index', array $data = array()): string
    {
        if (!isset(self::$routes[$controller][$action])) {
            throw new \Exception("Nelze sestavit neexistujici URL odkazu ($controller), akce: ($action).");
        }
        /** @var Route $route */
        $route = self::$routes[$controller][$action];

        return $route->createUrl($data);
    }

}