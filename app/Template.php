<?php

namespace App;

use App\Exceptions\NoTemplateFoundException;
use Exception;

class Template
{
    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string
     */
    private $templateDir;

    /**
     * @param string $filePath
     * @param null $data
     * @throws /Exception
     */
    public function __construct($filePath = null, $data = null)
    {
        $this->templateDir = ROOT_DIR . '/templates/';
        if ($filePath) {
            $this->setFilePath($filePath);
        }
        if ($data) {
            $this->addData($data);
        }
    }

    /**
     * @param string $filePath
     * @return $this
     * @throws /Exception
     */
    public function setFilePath($filePath)
    {
        $files = glob($this->templateDir . $filePath . '.php');
        if (!$files) {
            $files = glob($this->templateDir . $filePath);
        }

        if ($files) {
            $this->filePath = current($files);
        } else {
            throw new NoTemplateFoundException('No such file for use for template "' . $filePath . '"');
        }

        return $this;
    }


    /**
     * @param [] $data
     * @return $this
     */
    public function addData($data)
    {
        if ($data && !is_array($data)) {
            $data = array($data);
        }
        if ($data) {
            $this->data = array_merge($this->data, $data);
        }

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function render(array $data = [])
    {
        if (!$this->filePath) {
            throw new NoTemplateFoundException('FilePath of template not specified.');
        }

        $this->addData($data);

        $obLevel = ob_get_level();

        ob_start();

        if ($this->data) {
            extract($this->data, EXTR_OVERWRITE);
        }

        // We'll evaluate the contents of the view inside a try/catch block so we can
        // flush out any stray output that might get out before an error occurs or
        // an exception is thrown. This prevents any partial views from leaking.
        try {
            include $this->filePath;
        } catch (Exception $e) {
            $this->handleViewException($e, $obLevel);
        } catch (\Throwable $e) {
            //$this->handleViewException(new Framework\Exception\FatalException($e), $obLevel);
            $this->handleViewException(new Exception($e), $obLevel);
        }

        return ltrim(ob_get_clean());
    }

    /**
     * Handle a view exception.
     *
     * @param Exception $e
     * @param int $obLevel
     * @return void
     *
     * @throws Exception
     */
    protected function handleViewException(Exception $e, $obLevel)
    {
        while (ob_get_level() > $obLevel) {
            ob_end_clean();
        }

        throw $e;
    }
}