<?php

namespace App;

class Pager
{
    protected $total;
    protected $perPage;
    protected $page;

    public function __construct($perPage, $total, $page)
    {
        $this->perPage = $perPage;
        $this->total = $total;
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->page * $this->perPage;
    }

    /**
     * @return string|null
     */
    public function getNextUrl():? string
    {
        if ($this->total / $this->perPage > $this->page + 1) {
            $params = Router::$urlParams;
            $params['page'] = $this->page + 1;

            return Router::$url . '?' . http_build_query($params);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getPrevUrl():? string
    {
        if ($this->page - 1 > 0) {
            $params = Router::$urlParams;
            $params['page'] = $this->page - 1;

            return Router::$url . '?' . http_build_query($params);
        } else {
            if ($this->page == 1) {
                $params = Router::$urlParams;
                unset($params['page']);

                return Router::$url . '?' . http_build_query($params);
            }
        }

        return null;
    }

}