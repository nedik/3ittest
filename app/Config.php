<?php

namespace App;

class Config
{
    /**
     * @var Config
     */
    protected static $instance;
    protected $attributes;

    protected $envFile = ROOT_DIR . 'env.ini';

    public function __construct()
    {
        $this->parseEnvFile();
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public static function get($key)
    {
        $instance = self::getInstance();
        return $instance->$key;
    }

    public function __set($k, $v)
    {
        $this->attributes[$k] = $v;
    }

    public function __get($k)
    {
        if (!isset($this->attributes[$k])) {
            throw new \Exception($k.' - not such config key.');
        }

        return $this->attributes[$k];
    }

    protected function parseEnvFile()
    {
        $this->attributes = parse_ini_file($this->envFile);
    }
}