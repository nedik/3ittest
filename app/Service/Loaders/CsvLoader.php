<?php
namespace App\Service\Loaders;

class CsvLoader implements DataLoader
{
    /**
     * @var array
     */
    protected $csvData;

    public function load(string $filePath):bool
    {
        $fp = fopen($filePath, 'r');
        $firstRow = fgetcsv($fp);
        if (count($firstRow) == 4 && strtotime($firstRow[3])) {
            $this->csvData[] = $firstRow;
            while ($data = fgetcsv($fp)) {
                $this->csvData[] = $data;
            }
            return true;
        }

        return false;
    }


    public function getDataGenerator()
    {
        foreach ($this->csvData as $row) {
            $data = [
                'id' => $row[0],
                'jmeno' => $row[1],
                'prijmeni' => $row[2],
                'date' => $row[3],
            ];
            yield($data);
        }
    }
}