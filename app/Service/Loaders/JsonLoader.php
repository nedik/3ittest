<?php
namespace App\Service\Loaders;

class JsonLoader implements DataLoader
{
    /**
     * @var array
     */
    protected $json;

    public function load(string $filePath):bool
    {
        $this->json = json_decode(file_get_contents($filePath), true);
        return (bool) $this->json;
    }


    public function getDataGenerator()
    {
        foreach ($this->json as $data) {
            yield($data);
        }
    }
}