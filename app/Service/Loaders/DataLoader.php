<?php

namespace App\Service\Loaders;

interface DataLoader
{
    public function load(string $filePath):bool;

    public function getDataGenerator();
}