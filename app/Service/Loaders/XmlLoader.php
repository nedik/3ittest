<?php

namespace App\Service\Loaders;

class XmlLoader implements DataLoader
{

    protected $xml;

    public function load(string $filePath): bool
    {
        $this->xml = @simplexml_load_file($filePath);

        return (bool)$this->xml && isset($this->xml->ZAZNAM);
    }

    public function getDataGenerator()
    {
        foreach ($this->xml->ZAZNAM as $data) {
            yield ((array)$data);
        }
    }
}