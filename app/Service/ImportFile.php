<?php
namespace App\Service;

use App\Service\Loaders\DataLoader;
use dibi;

class ImportFile
{
    protected $rows = [];
    protected $loaders = [
        'XmlLoader',
        'CsvLoader',
        'JsonLoader',
    ];
    protected $possibleKeys = ['id' => 'int', 'date' => 'date', 'jmeno' => 'string', 'prijmeni' => 'string'];

    public function proccessImport($filePath): bool
    {
        foreach ($this->loaders as $loaderName) {
            $loaderClassName = '\\App\\Service\\Loaders\\'.$loaderName;
            /** @var DataLoader $loader */
            $loader = new $loaderClassName;
            if ($loader->load($filePath)) {
                $this->importData($loader);
                return true;
            }
        }

        return false;
    }

    private function importData(DataLoader $loader)
    {
        $data = $loader->getDataGenerator();
        foreach ($data as $row) {
            $this->parseToRows($row);
        }

        dibi::query('DELETE FROM [zaznamy]');
        dibi::query('INSERT INTO [zaznamy] %m', $this->rows);
    }

    protected function parseToRows($row)
    {
        foreach ($row as $k => $v) {
            $k = strtolower($k);
            if (array_key_exists($k, $this->possibleKeys)) {
                switch ($this->possibleKeys[$k]) {
                    case 'int':
                        $v = (int) $v;
                        break;
                    case 'date':
                        $v = date('Y-m-d', strtotime($v));
                        break;
                }
                if (!isset($this->rows[strtolower($k)])) {
                    $this->rows[strtolower($k)] = [];
                }
                $this->rows[strtolower($k)][] = $v;
            }
        }
    }
}