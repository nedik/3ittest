<?php

namespace App;

class Route
{
    /**
     * @var string
     */
    protected $url;
    /**
     * @var string
     */
    protected $controller;
    /**
     * @var string
     */
    protected $action;

    public function __construct($url, $controller, $action = 'index')
    {
        $this->url = $url;
        $this->controller = $controller;
        $this->action = $action;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param array $data
     * @return string
     */
    public function createUrl(array $data = []): string
    {
        $url = $this->url;
        $url = rtrim($url, '?&');
        $url = $this->addHttpQuery($url, $data);

        return $url;
    }

    /**
     * @param string $url
     * @param array $data
     * @return string
     */
    protected function addHttpQuery(string $url, array $data): string
    {
        $queryString = http_build_query($data);
        if ($queryString) {
            if (strpos($url, '?') === false) {
                $url = $url . '?' . $queryString;
            } else {
                $url = $url . '&' . $queryString;
            }
        }

        return $url;
    }
}