<?php
namespace App\Controllers;

use App\Message;
use App\Router;
use App\Service\ImportFile;
use App\View;

class Homepage
{
    public function index()
    {
        return ['message' => 'hello'];
    }


    public function saveFile()
    {
        $file = $_FILES['file'];
        if (!is_uploaded_file($file['tmp_name'])) {
            View::redirect(Router::url('homepage'))->withMessage('Zvolte prosím soubor k importu.', Message::ERROR);
            return ;
        }

        $service = new ImportFile();
        if ($service->proccessImport($file['tmp_name'])) {
            View::redirect(Router::url('dataview'))->withMessage('Data v pořádku nahrána do databáze');
        } else {
            View::redirect(Router::url('homepage'))->withMessage('Chyba nahrávání dat, zkontrolujte, jestli zadáváte správny soubor pro import..', Message::ERROR);
        }
    }
}