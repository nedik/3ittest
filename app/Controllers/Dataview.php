<?php
namespace App\Controllers;

use App\Pager;
use App\Router;
use dibi;

class Dataview
{
    protected $availableOrderBy = ['id', 'jmeno', 'prijmeni', 'date'];

    public function index()
    {
        $perPage = 13;
        $page = isset($_GET['page']) && (int)$_GET['page'] > 0 ? (int)$_GET['page'] : 0;
        $total = dibi::query('SELECT count(id) as total FROM [zaznamy]')->fetchSingle();

        $pager = new Pager($perPage, $total, $page);

        $items = dibi::query('SELECT * FROM [zaznamy] ORDER BY '.$this->getOrderBy().' LIMIT ? OFFSET ?', $perPage, $pager->getOffset())->fetchAll();

        $orderBy = explode(' ', $this->getOrderBy());

        return ['items' => $items, 'pager' => $pager, 'orderBy' => $orderBy[0], 'orderDesc' => $orderBy[1], 'orderDescRouter' => $orderBy[1] == 'desc' ? 'asc' : 'desc'];
    }


    private function getOrderBy()
    {
        $orderBy = 'date';
        if (isset(Router::$urlParams['orderBy']) && in_array(Router::$urlParams['orderBy'], $this->availableOrderBy)) {
            $orderBy = Router::$urlParams['orderBy'];
        }
        $orderDesc = isset(Router::$urlParams['orderDesc']) && Router::$urlParams['orderDesc'] == 'desc' ? 'desc' : 'asc';

        return $orderBy .' '.$orderDesc;
    }

}