<?php

namespace App;

class Message
{
    const ERROR = 'danger';
    const SUCCESS = 'success';

    public $type;
    public $message;

    public function __construct($msg, $type = self::SUCCESS)
    {
        $this->message = $msg;
        $this->type = $type;
    }
}