<form action="<?= App\Router::url('homepage', 'saveFile') ?>" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="exampleInputEmail1">Soubor k importu</label>
        <input type="file" name="file" class="form-control-file" id="file" placeholder="Zadej soubor (csv, json, xml)">
        <small id="emailHelp" class="form-text text-muted">Zadej soubor (csv, json, xml)</small>
    </div>

    <button type="submit" class="btn btn-primary">Odeslat soubor</button>

</form>