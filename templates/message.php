<?php /** @var \App\Message $message */
if ($message) { ?>

    <p class="alert alert-<?= $message->type ?>"><?= htmlspecialchars($message->message) ?></p>

<?php } ?>
