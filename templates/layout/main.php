<?php
use App\View;
/**
 * @var string $content
 */
?>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="robots" content="all, follow">
    <title><?= View::$title ?></title>
    <meta name="description" content="<?= View::$description ?>">
</head>

<body>
<div class="wrapper__main">

    <?= (new App\Template('head.php'))->render() ?>

    <div class="wrapper__content">
    <div class="wrapper__content--in">

        <?= (new App\Template('message.php'))->render(['message' => $message]) ?>

        <?= $content ?>

    </div>
    </div>

    <div class="clearfix"></div>

</div>

<script src="/js/css.bundle.js"></script>
<script src="/js/jsScript.bundle.js"></script>

</body>
</html>


