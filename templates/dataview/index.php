<?php

use App\Router;

if ($items) { ?>

    <table class="table table-hover ">
        <tr>
            <th>&nbsp;</th>
            <th><a href="<?= Router::url('dataview', 'index', ['orderBy' => 'date', 'orderDesc' => $orderDescRouter]) ?>">Datum <?= $orderBy == 'date' ? ($orderDesc == 'desc' ? '&uarr;' : '&darr;') : '' ?></a></th>
            <th><a href="<?= Router::url('dataview', 'index', ['orderBy' => 'jmeno', 'orderDesc' => $orderDescRouter]) ?>">Jméno <?= $orderBy == 'jmeno' ? ($orderDesc == 'desc' ? '&uarr;' : '&darr;') : '' ?></th>
            <th><a href="<?= Router::url('dataview', 'index', ['orderBy' => 'prijmeni', 'orderDesc' => $orderDescRouter]) ?>">Příjmení <?= $orderBy == 'prijmeni' ? ($orderDesc == 'desc' ? '&uarr;' : '&darr;') : '' ?></th>
        </tr>

        <?php foreach ($items as $item) { ?>
            <tr class="js-rowItem">
                <td><input type="checkbox" name="selected[]" value="<?= $item->id ?>"/></td>
                <td><?= htmlspecialchars($item->date->format('d.m.Y')) ?></td>
                <td><?= htmlspecialchars($item->jmeno) ?></td>
                <td><?= htmlspecialchars($item->prijmeni) ?></td>
            </tr>

        <?php } ?>

    </table>

    <?php if ($pager->getPrevUrl()) { ?>
        <a href="<?= $pager->getPrevUrl() ?>">Předchozí stránka</a>
    <?php } ?>

    <?php if ($pager->getNextUrl()) { ?>
        <a href="<?= $pager->getNextUrl() ?>">Další stránka</a>
    <?php } ?>


<?php } else { ?>
    <p class="alert alert-warning">Nic nenalezeno</p>
<?php } ?>
